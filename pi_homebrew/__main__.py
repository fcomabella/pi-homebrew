import logging
import logging.config
import os

import gi
import logdecorator

gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')

if __name__ == '__main__':
    from pi_homebrew.pihomebrewapp import PiHomebrewApp
    script_path = os.path.dirname(os.path.realpath(__file__))
    logging.config.fileConfig(f'{script_path}/logging.cfg')
    logging.getLogger('sqlalchemy.engine').setLevel(logging.WARN)

    @logdecorator.log_on_start(logging.INFO, 'Start application')
    @logdecorator.log_on_end(logging.INFO, 'Finish application')
    def start_main_loop() -> None:
        app = PiHomebrewApp()
        app.run()

    start_main_loop()
