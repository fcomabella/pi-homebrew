from pi_homebrew.models.enum import ApplianceType
from pi_homebrew.models.enum import TabType

from .dataclasses import ApplianceTypeDef

appliancetypedefs = (ApplianceTypeDef(ApplianceType.KETTLE,
                                      'kettle',
                                      (TabType.BREW_HOUSE,
                                       TabType.FERMENTATION)),
                     ApplianceTypeDef(ApplianceType.ACTUATOR,
                                      'actuator',
                                      (TabType.BREW_HOUSE,
                                       TabType.FERMENTATION)),
                     ApplianceTypeDef(ApplianceType.HEAT_EXCHANGER,
                                      'heat exchanger',
                                      (TabType.BREW_HOUSE,
                                       )),
                     ApplianceTypeDef(ApplianceType.FERMENTOR,
                                      'fermentor',
                                      (TabType.FERMENTATION,
                                       )))
