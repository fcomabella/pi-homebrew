import importlib
import logging
from dataclasses import dataclass
from typing import Any
from typing import Optional
from typing import Tuple
from typing import Type

from logdecorator import log_on_end
from logdecorator import log_exception

from pi_homebrew.models import Appliance
from pi_homebrew.models.enum import ApplianceType
from pi_homebrew.models.enum import TabType


@dataclass
class ApplianceTypeDef:
    value: ApplianceType
    name: str
    on_tabs: Tuple[TabType, ...]

    @property
    @log_on_end(logging.INFO, 'Space striped name: <{result}>.')
    def space_striped_name(self) -> str:
        return ''.join(name for name in self.name if not name.isspace())

    @property
    @log_on_end(logging.INFO, 'Pascal case name: <{result}>.')
    def pascal_case_name(self) -> str:
        return ''.join(name for name in self.name.title()
                       if not name.isspace())

    @property
    @log_on_end(logging.INFO, 'Model module name: <{result}>.')
    def model_module_name(self) -> str:
        return f'pi_homebrew.models.{self.space_striped_name.lower()}'

    @property
    @log_on_end(logging.INFO, 'Model module: <{result}>.')
    @log_exception(logging.ERROR,
                   'Error importing model module: <{self.model_module_name}>',
                   on_exceptions=(ModuleNotFoundError,
                                  ),
                   reraise=True)
    def model_module(self) -> Any:
        return importlib.import_module(self.model_module_name)

    @property
    @log_on_end(logging.INFO, 'Model class: <{result!r}>')
    def model_class(self) -> Optional[Type[Appliance]]:
        try:
            model_module = self.model_module
        except ModuleNotFoundError:
            return None
        else:
            return getattr(model_module, self.pascal_case_name)

    @property
    @log_on_end(logging.INFO, 'Model doc description.\n{result}')
    def docstring(self) -> str:
        if self.model_class is not None:
            return self.model_class.__doc__
        else:
            return ''
