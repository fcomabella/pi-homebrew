import logging
import os
from typing import Optional
from typing import Set
from typing import Tuple

from alembic import command as alembic_command
from alembic.config import Config as AlembicConfig
from alembic.runtime.migration import MigrationContext
from alembic.script import ScriptDirectory as AlembicScriptDirectory
from logdecorator import log_on_end
from logdecorator import log_exception
from logdecorator import log_on_start
from sqlalchemy import event as saevent
from sqlalchemy.engine import Engine as DBEngine
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import Session as DBSession
from sqlalchemy.orm import sessionmaker

from pi_homebrew.models import BaseModel


@saevent.listens_for(DBEngine, "connect")
@log_on_start(logging.INFO, 'Set foreign key constraints ON.')
@log_exception(logging.ERROR,
               'Error setting foreign key contrstraint ON.',
               on_exceptions=(Exception,
                              ),
               reraise=True)
def set_sqlite_pragma(dbapi_connection, connection_record) -> None:
    cursor = dbapi_connection.cursor()
    cursor.execute('PRAGMA foreign_keys=ON')
    cursor.close()


@log_on_start(logging.INFO, 'Connecting to database <{db_url}>')
def connect_db(db_url: str) -> Tuple[DBEngine, DBSession]:
    engine = create_engine(db_url, echo=False)
    db_session = sessionmaker(bind=engine)()
    return engine, db_session


@log_on_start(logging.INFO, 'Database file not found. Creating it.')
def create_database_file(engine: DBEngine, alembic_cfg: AlembicConfig) -> None:
    BaseModel.metadata.create_all(engine)
    set_database_revision(alembic_cfg)


@log_on_start(logging.INFO, 'Set database revision.')
def set_database_revision(alembic_cfg: AlembicConfig,
                          revision: str = 'head') -> None:
    alembic_command.stamp(alembic_cfg, revision)
    alembic_command.upgrade(alembic_cfg, revision)


@log_on_start(logging.INFO, 'Database found. Updating revision.')
def update_database_revision(engine: DBEngine,
                             alembic_cfg: AlembicConfig) -> Set[str]:
    last_revision = get_last_revision(alembic_cfg)
    current_revision = get_current_revision(engine)
    launch_revision_update(alembic_cfg, current_revision, last_revision)


@log_on_start(logging.INFO, 'Get last revision.')
@log_on_end(logging.INFO, 'Last revision: {result}')
def get_last_revision(alembic_cfg: AlembicConfig) -> Set[str]:
    directory = AlembicScriptDirectory.from_config(alembic_cfg)
    return set(directory.get_heads())


@log_on_start(logging.INFO, 'Get database current revision.')
@log_on_end(logging.INFO, 'Current revision <{result!r}>.')
def get_current_revision(engine: DBEngine) -> Optional[Set[str]]:
    current_revision = None

    with engine.begin() as connection:
        context = MigrationContext.configure(connection)
        current_revision = set(context.get_current_heads())

    return current_revision


@log_on_start(logging.INFO,
              ('Update database from revision {current_revision} to revision '
               '{last_revision}'))
def launch_revision_update(alembic_cfg,
                           current_revision,
                           last_revision) -> Set[str]:
    if last_revision != current_revision:
        alembic_command.upgrade(alembic_cfg, 'head')


@log_on_start(logging.INFO, 'Check database status')
def check_db(engine: DBEngine, db_filename: str, script_path: str) -> None:
    alembic_cfg = AlembicConfig(f'{script_path}/alembic.ini')

    if not os.path.exists(db_filename):
        create_database_file(engine, alembic_cfg)
    else:
        update_database_revision(engine, alembic_cfg)


@log_on_start(logging.INFO, 'Disposing SQLAlchemy engine.')
def dispose_engine(engine: DBEngine) -> None:
    engine.dispose()
