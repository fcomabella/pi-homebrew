from enum import IntEnum


class BrewSteps(IntEnum):
    IDLE = 0
    CHOOSING_RECIPE = 1
    PRE_HEATING = 2
    INFUSION = 3
    MASH = 4
    RECIRCULATION = 5
    SPARGING = 6
    BOILING = 7
    WHIRPOOL = 8
    COOLING = 9
    FERMENTING = 10
