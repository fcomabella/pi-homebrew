class DeleteError(Exception):
    """
    Raised when a model instance cannot be deleted.

    Parameters
    ----------

    message: str:
        The error message to display.

    model_instance: :obj:`BaseModel`:
        The BaseModel subclass instance that raised the exception.
    """
    pass
