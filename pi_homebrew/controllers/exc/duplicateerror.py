from .modelinstancefielderror import ModelInstanceFieldError


class DuplicateError(ModelInstanceFieldError):
    """
    Raised when the model instance is not unique

    Parameters
    ----------
    message: str:
        The error message to display.

    model_instance: :obj:`BaseModel`:
        The model instance with the invalid value.

    field_name: str:
        The model field with the invalid value.
    """
    pass
