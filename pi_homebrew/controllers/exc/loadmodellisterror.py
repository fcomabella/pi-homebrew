from .modeltypeerror import ModelTypeError


class LoadModelListError(ModelTypeError):
    """
    Raised when a list of model instances cannot be loaded

    Parameters
    ----------
    message: str:
        Error description.

    model_type: :obj:`Type[BaseModel]`:
        The SqlAlchemy BaseModel subclass affected by the error.
    """
    pass
