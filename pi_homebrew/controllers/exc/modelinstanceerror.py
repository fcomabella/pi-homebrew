from pi_homebrew.models import BaseModel


class ModelInstanceError(Exception):
    """
    Base exception class for errors related to model instances

    Parameters
    ----------

    message: str:
        The error message to display.

    model_instance: :obj:`BaseModel`:
        The BaseModel subclass instance that raised the exception.
    """
    def __init__(self, message: str, model_instance: BaseModel) -> None:
        super().__init__(message)
        self.message = message
        self.model_instance = model_instance
