from typing import Any, Type
from .modeltypeerror import ModelTypeError
from pi_homebrew.models import BaseModel


class NotFoundError(ModelTypeError):
    """
    Raised when a model instance cannot be found on the database.

    Parameters
    ----------
    message: str:
        Error description.

    model_type: :obj:`Type[BaseModel]`:
        The SqlAlchemy BaseModel subclass affected by the error.

    instance_id: :obj:`Any`:
        The model instance primary key value.
    """
    def __init__(self,
                 message: str,
                 model_type: Type[BaseModel],
                 instance_id: Any) -> None:
        super().__init__(message, model_type)
        self.instance_id = instance_id
