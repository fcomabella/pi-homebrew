from .modelinstanceerror import ModelInstanceError


class SaveError(ModelInstanceError):
    """
    Raised when a model instance cannot be saved.

    Parameters
    ----------
    message: str:
        Error description.

    model_instance: :obj:`BaseModel`:
        The model instance affected by the error.
    """
    pass
