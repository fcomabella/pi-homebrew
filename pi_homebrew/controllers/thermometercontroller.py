import logging
from concurrent import futures
from typing import Any
from typing import Dict
from typing import Optional
from typing import Set
from typing import Tuple

import owclient  # type:ignore
from logdecorator import log_exception
from logdecorator import log_on_end
from logdecorator import log_on_start
from sqlalchemy.exc import DBAPIError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import NoResultFound

from pi_homebrew.controllers.exc import CheckDuplicateError
from pi_homebrew.controllers.exc import DeleteError
from pi_homebrew.controllers.exc import DuplicateError
from pi_homebrew.controllers.exc import EmptyFieldError
from pi_homebrew.controllers.exc import LoadModelInstanceError
from pi_homebrew.controllers.exc import NotFoundError
from pi_homebrew.controllers.exc import SaveError
from pi_homebrew.controllers.enum import BrewSteps
from pi_homebrew.models import Appliance
from pi_homebrew.models import Thermometer


@log_on_start(logging.INFO, 'Check if path is used.')
@log_on_end(logging.INFO, 'Path is used {result}.')
@log_exception(logging.ERROR,
               'Error checking if path is already used',
               on_exceptions=(CheckDuplicateError,
                              ),
               reraise=True)
def is_used_path(db_session: Session,
                 to_check: Thermometer,
                 appliance: Appliance,
                 exclude: Optional[Thermometer] = None) -> bool:
    criterion: Tuple[Any,
                     ...] = (Thermometer.path == to_check.path,
                             Thermometer.appliance_id == appliance.id)

    if exclude is not None:
        criterion += (Thermometer.id != exclude.id, )

    try:
        with db_session.no_autoflush:
            found = db_session.query(Thermometer).filter(*criterion).count()
    except DBAPIError as exc:
        db_session.rollback()
        raise CheckDuplicateError(
            f'Error checking if path is already used.') from exc
    else:
        return found > 0


@log_on_start(logging.INFO, 'Check if name is unique.')
@log_exception(logging.ERROR,
               'Error checking thermometer unique name',
               on_exceptions=(CheckDuplicateError,
                              ),
               reraise=True)
@log_on_end(logging.INFO, 'Is unique name {result}')
def is_unique_name(db_session: Session, thermometer: Thermometer) -> bool:
    try:
        with db_session.no_autoflush:
            found = db_session.query(Thermometer).filter(
                Thermometer.name == thermometer.name,
                Thermometer.id != thermometer.id,
                Thermometer.appliance_id == thermometer.appliance_id).count()
    except DBAPIError as exc:
        db_session.rollback()
        raise CheckDuplicateError(
            'Error checking thermometer unique name') from exc
    else:
        return found == 0


@log_on_start(logging.INFO, 'Checking if thermometer path is unique.')
@log_exception(logging.ERROR,
               'Error checking if path is unique.',
               on_exceptions=(CheckDuplicateError,
                              ),
               reraise=True)
@log_on_end(logging.INFO, 'Is unique {result}')
def is_unique_path(db_session: Session, thermometer: Thermometer) -> bool:
    try:
        with db_session.no_autoflush:
            found = db_session.query(Thermometer).filter(
                Thermometer.path == thermometer.path,
                Thermometer.id != thermometer.id,
                Thermometer.appliance_id == thermometer.appliance_id).count()
    except DBAPIError as exc:
        db_session.rollback()
        raise CheckDuplicateError('Error checking if path is unique',
                                  Thermometer) from exc
    else:
        return found == 0


@log_on_start(logging.INFO, 'Merge thermometer {thermometer!r}')
@log_on_end(logging.INFO, 'Merged thermometer {result!r}')
def merge_thermometer(db_session: Session,
                      thermometer: Thermometer) -> Thermometer:
    if thermometer.id is not None:
        return db_session.merge(thermometer)

    return thermometer


@log_on_start(logging.INFO, 'Save thermometer {thermometer!r}')
@log_on_end(logging.INFO, 'Thermometer {result!r} saved successfuly.')
@log_exception(logging.ERROR,
               'Error saving thermometer {thermometer!r}',
               on_exceptions=(EmptyFieldError,
                              DuplicateError,
                              SaveError),
               reraise=True)
def save_thermometer(db_session: Session,
                     thermometer: Thermometer) -> Thermometer:
    thermometer = merge_thermometer(db_session, thermometer)
    db_session.add(thermometer)

    try:
        db_session.commit()
    except IntegrityError as exc:
        db_session.rollback()
        message = str(exc)

        if ('NOT NULL constraint failed' in message
                or 'CHECK constraint failed: thermometers' in message):
            msg = ''
            field_name = ''

            if 'thermometers.appliance_id' in message:
                msg = 'A Thermometer must have an associated Appliance'
                field_name = 'appliance_id'
            elif 'thermometers.name' in message or thermometer.name == '':
                msg = 'A Thermometer must have a name'
                field_name = 'name'
            elif 'thermometers.path' in message or thermometer.path == '':
                msg = 'A thermometer must have a device path.'
                field_name = 'path'

            raise EmptyFieldError(msg, thermometer, field_name) from exc

        if 'UNIQUE constraint failed:' in message:
            msg = ''
            field_name = ''

            if 'thermometers.name' in message:
                msg = 'Duplicate thermometer name'
                field_name = 'name'

            if 'thermometers.path' in message:
                msg = 'Duplicate thermometer path'
                field_name = 'path'

            raise DuplicateError(msg, thermometer, field_name) from exc

        if 'FOREIGN KEY constraint failed' in message:
            msg = f'Incorrect appliance_id {thermometer.appliance_id}'
            raise SaveError(msg, thermometer)

        msg = f'Error saving the thermometer {thermometer}'
        raise SaveError(msg, thermometer) from exc
    except DBAPIError as exc:
        db_session.rollback()
        msg = f'DBAPIerror saving thermometer {thermometer}'
        raise SaveError(msg, thermometer) from exc
    else:
        return thermometer


@log_on_start(logging.INFO, 'Get thermometer {thermometer_id:d}')
@log_on_end(logging.INFO, 'Thermometer loaded {result!r}')
@log_exception(logging.ERROR,
               'Error getting thermometer.',
               on_exceptions=(NotFoundError,
                              LoadModelInstanceError),
               reraise=True)
def get_thermometer(db_session: Session, thermometer_id: int) -> Thermometer:
    try:
        thermometer = db_session.query(Thermometer).filter(
            Thermometer.id == thermometer_id).one()
    except NoResultFound as exc:
        db_session.rollback()
        msg = f'Thermometer <{thermometer_id}> not found.'
        raise NotFoundError(msg, Thermometer, thermometer_id) from exc
    except DBAPIError as exc:
        db_session.rollback()
        msg = 'Error loading the thermometer.'
        raise LoadModelInstanceError(msg, Thermometer, thermometer_id) from exc
    else:
        return thermometer


@log_on_start(logging.INFO, 'Delete thermometer {thermometer!r}.')
@log_on_end(logging.INFO, 'Thermometer successfully deleted.')
@log_exception(logging.ERROR,
               'Error deleting thermometer.',
               on_exceptions=(DeleteError,
                              ),
               reraise=True)
def delete_thermometer(db_session: Session, thermometer: Thermometer) -> None:
    try:
        db_session.delete(thermometer)
        db_session.commit()
    except DBAPIError as exc:
        db_session.rollback()
        msg = f'Error deleting thermometer.'
        raise DeleteError(msg) from exc


@log_on_start(logging.INFO, 'Load thermometer devices.')
def load_thermometer_devices(
        paths: Optional[Set[str]] = None) -> futures.Future:
    @log_on_end(logging.INFO, 'Thermometers loaded {result!r}.')
    @log_exception(logging.ERROR,
                   'Error loading thermometers.',
                   on_exceptions=(Exception,
                                  ),
                   reraise=True)
    def load_ow_thermometers(
        paths: Optional[Set[str]] = None
    ) -> Dict[str,
              owclient.devices.Thermometer]:
        owc = owclient.OwClient()

        if paths is None:
            thermometers = {
                thermometer.path: thermometer
                for thermometer in owc.devices
                if isinstance(thermometer,
                              owclient.devices.Thermometer)
            }
        else:
            thermometers = {path: owc.load_device(path) for path in paths}

        return thermometers

    executor: futures.Executor = futures.ThreadPoolExecutor(max_workers=1)
    future = executor.submit(load_ow_thermometers, paths)
    executor.shutdown(True)

    return future


@log_on_start(logging.INFO, 'Refresh thermometer {thermometer!r}.')
@log_on_end(logging.INFO, 'Thermometer {thermometer!r} refreshed.')
@log_exception(logging.ERROR,
               'Error refreshing thermometer.',
               on_exceptions=(Exception,
                              ),
               reraise=True)
def refresh_thermometer(db_session: Session,
                        thermometer: Thermometer) -> Thermometer:
    db_session.rollback()
    return thermometer


def get_step_thermometers(db_session: Session, step: BrewSteps) -> Set[str]:
    return {
        thermometer.path
        for thermometer in db_session.query(Thermometer.path).distinct().all()
    }
