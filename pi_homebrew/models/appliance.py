from typing import TYPE_CHECKING

from sqlalchemy import CheckConstraint
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import Unicode
from sqlalchemy import UnicodeText
from sqlalchemy import UniqueConstraint
from sqlalchemy.orm import relationship

from pi_homebrew.models import BaseModel

if TYPE_CHECKING:
    from pi_homebrew.models import Thermometer  # noqa F401


class Appliance(BaseModel):
    """
    An appliance, assigned to a tab.

    The appliance model is used to derive all the appliances a tab can have
    assigned

    Attributes
    ----------
    id : int:
        Primary Key.

    appliance_type : int:
        This attribute holds the appliance type value. See AplianceType enum
        for details.

    name: str:
        The appliance name. It must be unique on the tab the appliance is
        placed.

    description : str, optional:
        The appliance description allows you to write a longer text describing
        the appliance function.

    thermometers : :obj:`list` of :obj:`Thermometer`:
        The assigned thermometers list.
    """
    __tablename__ = 'appliances'
    __table_args__ = (UniqueConstraint('on_tab',
                                       'name',
                                       name='uix_appliance_name'),
                      )

    id = Column(Integer, primary_key=True)
    appliance_type = Column(Integer, nullable=False)
    on_tab = Column(Integer, nullable=False)
    name = Column(Unicode(50),
                  CheckConstraint("name<>''"),
                  nullable=False,
                  index=True)
    description = Column(UnicodeText, nullable=False, default='')

    thermometers = relationship('Thermometer',
                                order_by='Thermometer.id',
                                back_populates='appliance',
                                cascade='all, delete, delete-orphan')

    __mapper_args__ = {
        'polymorphic_on': appliance_type,
    }

    def __repr__(self):
        return (f'<{type(self).__name__} ('
                f'id={self.id}, '
                f'on_tab={self.on_tab}, '
                f'name={self.name}, '
                f'description={self.description}, '
                f'type={self.appliance_type}'
                f')>')
