from enum import IntEnum


class TabType(IntEnum):
    BREW_HOUSE = 0
    FERMENTATION = 1
