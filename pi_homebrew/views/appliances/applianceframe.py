import logging
from typing import TYPE_CHECKING
from typing import Callable
from typing import Dict
from typing import Optional

from gi.repository import GLib
from gi.repository import Gtk
from logdecorator import log_on_start

from pi_homebrew.models import Appliance as ApplianceModel
from pi_homebrew.models import Thermometer as ThermometerModel

if TYPE_CHECKING:
    from pi_homebrew.views import MainWindow


class ApplianceFrame(Gtk.Frame):
    @log_on_start(logging.INFO, 'Initializing main window')
    def __init__(self, parent: 'MainWindow') -> None:
        Gtk.Frame.__init__(self)

        self.parent = parent
        self.temperature_entries: Dict[str, Gtk.Entry] = dict()
        self.cancel_entry_updates: Optional[Callable[[], None]] = None

        self.set_label_align(0.5, 0.5)

        self.controls_box = Gtk.Box(spacing=4)
        self.add(self.controls_box)

        self.temperatures_grid = Gtk.Grid(row_homogeneous=False,
                                          row_spacing=4,
                                          column_spacing=4,
                                          margin=4)

        self.controls_box.pack_end(self.temperatures_grid, True, True, 0)

    @log_on_start(logging.INFO, 'Set appliance {appliance!r}')
    def set_appliance(self,
                      appliance: ApplianceModel,
                      draw_thermometers: bool = True) -> None:
        self.appliance = appliance
        self.set_label(appliance.name)
        if draw_thermometers is True:
            self.draw_thermometers()

    @log_on_start(logging.INFO, 'Clear thermometers grid')
    def clear_thermometers_grid(self) -> None:
        if self.cancel_entry_updates is not None:
            self.cancel_entry_updates()

        while self.temperatures_grid.remove_row(1):
            pass

    @log_on_start(logging.INFO, 'Draw thermometers.')
    def draw_thermometers(self) -> None:
        self.clear_thermometers_grid()

        thermometer: ThermometerModel
        for row, thermometer in enumerate(self.appliance.thermometers):
            self.draw_thermometer_row(thermometer, row)

        self.cancel_entry_updates = self.parent.add_on_read_done(
            self.update_entries)

    @log_on_start(logging.INFO,
                  'Loading display for thermometer: {thermometer!r}.')
    def draw_thermometer_row(self,
                             thermometer: ThermometerModel,
                             row: int) -> None:
        name_label = Gtk.Label(thermometer.name, xalign=1)

        temperature_entry = Gtk.Entry(editable=False,
                                      sensitive=False,
                                      xalign=1,
                                      hexpand=True,
                                      width_chars=8)

        if thermometer.description is not None:
            temperature_entry.set_tooltip_text(thermometer.description)

        self.temperatures_grid.attach(name_label, 0, row, 1, 1)
        self.temperatures_grid.attach(temperature_entry, 1, row, 1, 1)

        self.temperature_entries[thermometer.path] = temperature_entry

    @log_on_start(logging.INFO, 'Update thermometer entries {temperatures!r}.')
    def update_entries(self, temperatures: Dict[str, Optional[float]]) -> None:
        for path, entry in self.temperature_entries.items():
            temperature = temperatures.get(path, None)
            if temperature is not None:
                GLib.idle_add(entry.set_text, f'{temperature:.2f}C')
            else:
                GLib.idle_add(entry.set_text, 'N/A')
