import logging
import statistics
from typing import TYPE_CHECKING
from typing import Callable
from typing import Dict
from typing import Optional

from gi.repository import GLib
from gi.repository import Gtk
from logdecorator import log_on_start

from pi_homebrew.models import Kettle as KettleModel
from pi_homebrew.views import NotificationBox
from pi_homebrew.views.appliances import ApplianceFrame

if TYPE_CHECKING:
    from pi_homebrew.views import MainWindow


class KettleFrame(ApplianceFrame):
    @log_on_start(logging.INFO, 'Initialize KettleFrame. Parent:{parent!r}')
    def __init__(self, parent: 'MainWindow') -> None:
        super().__init__(parent)
        self.average_entry: Optional[Gtk.Entry] = None
        self.cancel_average_update: Optional[Callable[[], None]] = None

    @log_on_start(logging.INFO, 'Set kettle {kettle!r}')
    def set_appliance(self, kettle: KettleModel) -> None:
        super().set_appliance(kettle, kettle.show_all_thermometers)

        thermometer_count = len(kettle.thermometers)
        if thermometer_count == 0:
            self.draw_no_thermometers()
        elif thermometer_count > 1 and kettle.show_average_temp:
            self.draw_average_row(thermometer_count)

    @log_on_start(logging.INFO, 'Draw no thermometers.')
    def draw_no_thermometers(self) -> None:
        no_thermometers_box = NotificationBox('No thermometers registered',
                                              'dialog-warning',
                                              vexpand=True,
                                              hexpand=True)
        self.temperatures_grid.attach(no_thermometers_box, 0, 0, 0, 0)
        no_thermometers_box.show_all()

    @log_on_start(logging.INFO, 'Draw average row on row {row:d}.')
    def draw_average_row(self, row: int) -> None:
        average_label = Gtk.Label('Average', xalign=1)
        self.average_entry = Gtk.Entry(editable=False,
                                       sensitive=False,
                                       hexpand=True,
                                       width_chars=8,
                                       xalign=1)
        self.temperatures_grid.attach(average_label, 0, row, 1, 1)
        self.temperatures_grid.attach(self.average_entry, 1, row, 1, 1)
        self.cancel_average_update = self.parent.add_on_read_done(
            self.update_average)

    @log_on_start(logging.INFO, 'Update average row. {temperatures!r}')
    def update_average(self, temperatures: Dict[str, Optional[float]]) -> None:
        paths = [
            thermometer.path for thermometer in self.appliance.thermometers
        ]
        valid_temperatures = [
            temperature for (path,
                             temperature) in temperatures.items()
            if temperature is not None and path in paths
        ]
        if self.average_entry is not None:
            if len(valid_temperatures) > 0:
                average = statistics.mean(valid_temperatures)
                GLib.idle_add(self.average_entry.set_text, f'{average:.2f}C')
            else:
                GLib.idle_add(self.average_entry.set_text, 'N/A')
