import logging

from gi.repository import Gtk
from logdecorator import log_on_start

from pi_homebrew.views.enum import DialogButtonType


class ConfirmDialog(Gtk.MessageDialog):
    @log_on_start(logging.INFO, 'Initializing confirm dialog.')
    def __init__(self, title: str, text: str, **kwargs) -> None:
        buttons = kwargs.get('buttons',
                             DialogButtonType.YES + DialogButtonType.NO)
        message_type = kwargs.get('message_type', Gtk.MessageType.QUESTION)

        if 'buttons' in kwargs:
            del kwargs['buttons']

        kwargs['title'] = title
        kwargs['text'] = text
        kwargs['message_type'] = message_type
        Gtk.MessageDialog.__init__(self, **kwargs)
        self.set_modal(True)

        hasAccept = (buttons & DialogButtonType.ACCEPT)
        hasYes = (buttons & DialogButtonType.YES)
        hasOk = (buttons & DialogButtonType.OK)
        hasCancel = (buttons & DialogButtonType.CANCEL)
        hasNo = (buttons & DialogButtonType.NO)
        hasClose = (buttons & DialogButtonType.CLOSE)
        hasRetry = (buttons & DialogButtonType.RETRY)
        hasSave = (buttons & DialogButtonType.SAVE)
        hasDontSave = (buttons & DialogButtonType.DONT_SAVE)

        if hasAccept is DialogButtonType.ACCEPT:
            self.add_button('Accept', DialogButtonType.ACCEPT)

        if hasYes is DialogButtonType.YES:
            self.add_button('Yes', DialogButtonType.YES)

        if hasOk is DialogButtonType.OK:
            self.add_button('Ok', DialogButtonType.OK)

        if hasRetry is DialogButtonType.RETRY:
            self.add_button('Retry', DialogButtonType.RETRY)

        if hasDontSave is DialogButtonType.DONT_SAVE:
            self.add_button("Don't save", DialogButtonType.DONT_SAVE)

        if hasCancel is DialogButtonType.CANCEL:
            self.add_button('Cancel', DialogButtonType.CANCEL)

        if hasNo is DialogButtonType.NO:
            self.add_button('No', DialogButtonType.NO)

        if hasClose is DialogButtonType.CLOSE:
            self.add_button('Close', DialogButtonType.CLOSE)

        if hasSave is DialogButtonType.SAVE:
            self.add_button('Save', DialogButtonType.SAVE)
