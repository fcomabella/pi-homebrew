import importlib
import logging
from dataclasses import dataclass
from typing import TYPE_CHECKING
from typing import Any
from typing import Optional
from typing import Type

from logdecorator import log_on_end
from logdecorator import log_exception

from pi_homebrew.controllers.dataclasses import ApplianceTypeDef

if TYPE_CHECKING:
    from pi_homebrew.views.editors.editappliancebox import EditApplianceBox  # noqa F401


@dataclass
class ApplianceViewTypeDef(ApplianceTypeDef):
    @property
    @log_on_end(logging.INFO, 'Editor module name: <{result}>')
    def editor_module_name(self) -> str:
        return (f'pi_homebrew.views.editors.edit'
                f'{self.space_striped_name.lower()}box')

    @property
    @log_on_end(logging.INFO, 'Editor module: <{result!r}>')
    @log_exception(logging.ERROR,
                   'Error importing module: <{self.editor_module_name}>',
                   on_exceptions=(ModuleNotFoundError,
                                  ),
                   reraise=True)
    def editor_module(self) -> Optional[Any]:
        return importlib.import_module(self.editor_module_name)

    @property
    @log_on_end(logging.INFO, 'Editor class: <{result!r}>')
    def editor_class(self) -> Optional[Type['EditApplianceBox']]:
        try:
            editor_module = self.editor_module
        except ModuleNotFoundError:
            return None
        else:
            editor_class_name = f'Edit{self.pascal_case_name}Box'
            return getattr(editor_module, editor_class_name)
