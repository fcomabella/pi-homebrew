from dataclasses import dataclass
from typing import Callable
from typing import Optional

from gi.repository import Gtk


@dataclass
class WidgetEventHandler:
    """
    Keeps track of events to connect and disconnect from a Gtk.Widget
    """
    widget: Gtk.Widget
    event_name: str
    handler_func: Callable[..., None]
    handler_id: Optional[int] = None
