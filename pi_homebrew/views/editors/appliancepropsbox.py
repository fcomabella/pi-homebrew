import logging
from typing import TYPE_CHECKING
from typing import Optional

from gi.repository import Gtk
from logdecorator import log_on_end
from logdecorator import log_on_start

from pi_homebrew.controllers import appliancecontroller
from pi_homebrew.controllers.appliancetypedefs import appliancetypedefs
from pi_homebrew.models import Appliance
from pi_homebrew.models.enum import TabType
from pi_homebrew.views.dataclasses.widgeteventhandler import WidgetEventHandler

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp


class AppliancePropsBox(Gtk.Box):
    @log_on_end(logging.INFO, ('Initialize AppliancePropsBox. <{self!r}>'))
    def __init__(self,
                 application: 'PiHomebrewApp',
                 parent: Gtk.Widget) -> None:
        Gtk.Box.__init__(self,
                         orientation=Gtk.Orientation.VERTICAL,
                         spacing=4,
                         border_width=8)

        self.appliance: Optional[Appliance] = None
        self.application = application
        self.parent = parent

        tab_label = Gtk.Label(label='Tab:', xalign=0)
        self.pack_start(tab_label, False, False, 0)

        self.tab_entry = Gtk.Entry(sensitive=False)
        self.pack_start(self.tab_entry, False, False, 0)

        name_label = Gtk.Label('Name:', xalign=0)
        self.pack_start(name_label, False, False, 0)

        self.name_entry = Gtk.Entry(max_length=50)
        self.name_entry.set_icon_from_icon_name(
            Gtk.EntryIconPosition.SECONDARY,
            'emblem-important-symbolic')
        self.pack_start(self.name_entry, False, False, 0)

        description_scroll_window = Gtk.ScrolledWindow(
            hscrollbar_policy=Gtk.PolicyType.AUTOMATIC,
            vscrollbar_policy=Gtk.PolicyType.AUTOMATIC,
            shadow_type=Gtk.ShadowType.IN)
        self.description_text_buffer = Gtk.TextBuffer()
        description_text_view = Gtk.TextView(
            buffer=self.description_text_buffer,
            monospace=True,
            wrap_mode=Gtk.WrapMode.WORD,
            hexpand=True)
        description_scroll_window.add(description_text_view)
        self.pack_end(description_scroll_window, True, True, 0)

        description_label = Gtk.Label(label='Description:', xalign=0)
        self.pack_end(description_label, False, False, 0)

        self.handlers = (WidgetEventHandler(self.name_entry,
                                            'changed',
                                            self.name_entry_on_changed),
                         )

    @log_on_start(logging.INFO, 'Disconnect handlers')
    def disconnect_handlers(self) -> None:
        for handler in (handler for handler in self.handlers
                        if handler.handler_id is not None):
            handler.widget.disconnect(handler.handler_id)
            handler.handler_id = None

    @log_on_start(logging.INFO, 'Connect handlers')
    def connect_handlers(self) -> None:
        for handler in self.handlers:
            handler.handler_id = handler.widget.connect(
                handler.event_name,
                handler.handler_func)

    @log_on_start(logging.INFO, 'Set appliance: {appliance!r}')
    def set_appliance(self,
                      appliance: Appliance,
                      manage_handlers: bool = False) -> None:
        """
        Sets the appliance

        Parameters
        ----------
        appliance: Appliance
            The appliance model instance
        """
        if manage_handlers is True:
            self.disconnect_handlers()

        self.appliance = appliance

        if appliance.on_tab == TabType.BREW_HOUSE:
            self.tab_entry.set_text('Brew house')
        elif appliance.on_tab == TabType.FERMENTATION:
            self.tab_entry.set_text('Fermentation')

        if appliance.name is not None:
            self.name_entry.set_text(appliance.name)
        else:
            self.name_entry.set_text('')

        self.__update_name_entry_icon()
        self.__update_name_entry_tooltip()

        if appliance.description is not None:
            self.description_text_buffer.set_text(appliance.description)
        else:
            self.description_text_buffer.set_text('')

        if manage_handlers is True:
            self.connect_handlers()

    @log_on_start(logging.INFO, 'Name changed to: {entry.props.text}')
    def name_entry_on_changed(self, entry: Gtk.Entry) -> None:
        if self.appliance is not None:
            name = entry.get_text().strip()
            self.appliance.name = name

            self.__update_name_entry_icon(entry)
            self.__update_name_entry_tooltip(entry)

            self.parent.appliance_on_change(self.appliance)

    @log_on_start(logging.INFO, 'Update name entry icon.')
    def __update_name_entry_icon(self,
                                 entry: Optional[Gtk.Entry] = None) -> None:
        if self.appliance is not None:
            if entry is None:
                entry = self.name_entry

            if self.appliance.name == '':
                icon_name = 'emblem-important-symbolic'
            else:
                if appliancecontroller.is_unique_name(
                        self.application.db_session,
                        self.appliance) is True:
                    icon_name = 'emblem-ok-symbolic'
                else:
                    icon_name = 'emblem-important-symbolic'

            entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY,
                                          icon_name)

    @log_on_start(logging.INFO, 'Update name entry tooltip.')
    def __update_name_entry_tooltip(self,
                                    entry: Optional[Gtk.Entry] = None) -> None:
        if self.appliance is not None:
            type_name = appliancetypedefs[self.appliance.appliance_type].name

            if entry is None:
                entry = self.name_entry

            if self.appliance.name == '':
                tooltip_text = f'The {type_name} must have a name'
            else:
                if appliancecontroller.is_unique_name(
                        self.application.db_session,
                        self.appliance) is True:
                    tooltip_text = None
                else:
                    tooltip_text = (
                        f'The name {self.appliance.name} is already used.')

            entry.set_tooltip_text(tooltip_text)

    @property
    @log_on_end(logging.INFO, 'Unsaved changes: {result}.')
    def unsaved_changes(self) -> bool:
        if self.appliance is not None:
            return self.application.db_session.is_modified(self.appliance)
        else:
            return False

    @property
    @log_on_end(logging.INFO, 'Is valid: {result}')
    def is_valid(self) -> bool:
        if self.appliance.name is None or self.appliance.name == '':
            return False

        if not appliancecontroller.is_unique_name(self.application.db_session,
                                                  self.appliance):
            return False

        if self.appliance.on_tab is None:
            return False

        return True
