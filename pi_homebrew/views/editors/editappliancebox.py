from typing import TYPE_CHECKING
from typing import Optional

from gi.repository import Gtk

from pi_homebrew.controllers import appliancecontroller
from pi_homebrew.models import Appliance
from pi_homebrew.views.editors.thermometersbox import ThermometersBox

import logging
from logdecorator import log_on_start, log_on_end

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp
    from pi_homebrew.views import PanedConfig


class EditApplianceBox(Gtk.Box):
    @log_on_start(
        logging.INFO,
        ('Initializing EditApplianceBox. application: {application!r}. '
         'parent: {parent!r}'))
    def __init__(self, application: 'PiHomebrewApp', parent: 'PanedConfig'):
        Gtk.Box.__init__(self,
                         orientation=Gtk.Orientation.VERTICAL,
                         border_width=8,
                         spacing=8)

        self.appliance: Optional[Appliance] = None
        self.application = application
        self.parent = parent

        self.notebook = Gtk.Notebook()
        self.pack_start(self.notebook, True, True, 0)

        self.thermometers_box = ThermometersBox(self.application, self)
        self.notebook.append_page(self.thermometers_box,
                                  Gtk.Label.new('Thermometers'))

        button_box = Gtk.ButtonBox(orientation=Gtk.Orientation.HORIZONTAL,
                                   layout_style=Gtk.ButtonBoxStyle.END,
                                   spacing=8)
        self.pack_end(button_box, False, False, 0)

        save_icon = Gtk.Image.new_from_icon_name('document-save-symbolic',
                                                 Gtk.IconSize.BUTTON)
        self.save_button = Gtk.Button(label='Save',
                                      sensitive=False,
                                      always_show_image=True,
                                      image=save_icon)
        self.save_button.connect('clicked', self.save_button_on_clicked)
        button_box.pack_start(self.save_button, False, False, 0)

        cancel_icon = Gtk.Image.new_from_icon_name('edit-undo-symbolic',
                                                   Gtk.IconSize.BUTTON)
        self.undo_button = Gtk.Button(label='Undo changes',
                                      sensitive=False,
                                      always_show_image=True,
                                      image=cancel_icon)
        self.undo_button.connect('clicked', self.undo_button_on_clicked)
        button_box.pack_start(self.undo_button, False, False, 0)

    @log_on_start(logging.INFO, 'Save button clicked.')
    def save_button_on_clicked(self, button: Gtk.Button) -> None:
        self.save_appliance()

    @log_on_start(logging.INFO, 'Set appliance. {appliance!r}')
    def set_appliance(self, appliance: Appliance) -> None:
        self.appliance = appliance
        self.props_box.set_appliance(appliance)
        self.thermometers_box.set_appliance(appliance)
        self.update_save_button_status()
        self.update_undo_button_status()

    @log_on_start(logging.INFO, 'Undo button clicked.')
    def undo_button_on_clicked(self, button: Gtk.Button) -> None:
        self.refresh_appliance()

    @log_on_end(logging.INFO,
                'Save button status {self.save_button.props.sensitive}.')
    def update_save_button_status(self) -> None:
        self.save_button.set_sensitive(self.is_valid)

    @log_on_end(logging.INFO,
                'Undo button status {self.undo_button.props.sensitive}.')
    def update_undo_button_status(self) -> None:
        self.undo_button.set_sensitive(self.unsaved_changes)

    @log_on_start(logging.INFO, 'Appliance changed to {appliance!r}.')
    def appliance_on_change(self, appliance: Appliance) -> None:
        self.parent.appliance_on_change(appliance)
        self.update_undo_button_status()
        self.update_save_button_status()

    @log_on_end(logging.INFO, 'Appliance refreshed {self.appliance!r}.')
    def refresh_appliance(self) -> None:
        if self.appliance is not None:
            self.app = appliancecontroller.refresh_apliance(
                self.application.db_session,
                self.appliance)
            self.thermometers_box.refresh_thermometer()
            self.set_appliance(self.appliance)

    @property
    @log_on_end(logging.INFO, 'Unsaved changes {result}')
    def unsaved_changes(self) -> bool:
        return (self.props_box.unsaved_changes
                or self.thermometers_box.unsaved_changes)

    @property
    @log_on_end(logging.INFO, 'Is valid {result}')
    def is_valid(self) -> bool:
        return (self.props_box.is_valid and self.thermometers_box.is_valid)
