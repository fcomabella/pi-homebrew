import logging
from typing import TYPE_CHECKING
from typing import Optional

from gi.repository import Gtk
from logdecorator import log_exception
from logdecorator import log_on_end
from logdecorator import log_on_start

from pi_homebrew.controllers import kettlecontroller
from pi_homebrew.models import Kettle
from pi_homebrew.views.editors.editappliancebox import EditApplianceBox
from pi_homebrew.views.editors.kettlepropsbox import KettlePropsBox

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp
    from pi_homebrew.views import PanedConfig


class EditKettleBox(EditApplianceBox):
    @log_on_start(logging.INFO, 'Initializing EditKettleBox.')
    def __init__(self, application: 'PiHomebrewApp', parent: 'PanedConfig'):
        super().__init__(application, parent)

        self.appliance: Optional[Kettle] = None
        self.props_box = KettlePropsBox(application, self)
        self.notebook.insert_page(self.props_box,
                                  Gtk.Label.new('Properties'),
                                  0)

    @log_on_start(logging.INFO, 'Save appliance {self.appliance!r}.')
    @log_on_end(logging.INFO, 'Appliance saved {self.appliance!r}.')
    @log_exception(logging.ERROR, 'Error saving apliance {self.appliance!r}.')
    def save_appliance(self) -> Optional[Kettle]:
        if self.appliance is not None:
            self.appliance = kettlecontroller.save_kettle(
                self.application.db_session,
                self.appliance)
            self.parent.appliance_on_change(self.appliance)
            return self.appliance

    @log_on_start(logging.INFO, 'Set kettle {kettle!r}')
    def set_appliance(self, kettle: Kettle) -> None:
        if kettle.id is None:
            kettle.show_all_thermometers = True
            kettle.show_average_temp = True

        super().set_appliance(kettle)
