import logging
from typing import TYPE_CHECKING
from typing import Callable
from typing import Dict
from typing import Optional
from typing import Tuple

from gi.repository import Gdk
from gi.repository import Gtk
from logdecorator import log_on_end
from logdecorator import log_on_start

from pi_homebrew.controllers.enum import BrewSteps
from pi_homebrew.views import AppliancesNotebook
from pi_homebrew.views import ConfirmDialog
from pi_homebrew.views import PanedConfig
from pi_homebrew.views.enum import DialogButtonType

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp


class MainWindow(Gtk.ApplicationWindow):
    """
        The application main window
    """
    @log_on_start(logging.INFO, 'Initialize main window')
    def __init__(self, application: 'PiHomebrewApp', **kwargs):
        self.application = application
        self.exit_cases = (DialogButtonType.YES,
                           DialogButtonType.DONT_SAVE,
                           DialogButtonType.SAVE)

        kwargs['application'] = self.application

        Gtk.ApplicationWindow.__init__(self, **kwargs)

        self.set_wmclass('Pi Homebrew', 'Pi Homebrew')

        self.header_bar = Gtk.HeaderBar(title='Pi Homebrew')
        self.header_bar.set_show_close_button(True)
        self.set_titlebar(self.header_bar)

        toolbar = Gtk.Toolbar(show_arrow=True,
                              toolbar_style=Gtk.ToolbarStyle.ICONS,
                              icon_size=Gtk.IconSize.BUTTON)
        self.header_bar.pack_end(toolbar)

        self.config_button = Gtk.ToolButton(
            sensitive=False,
            icon_name='document-page-setup-symbolic',
            tooltip_text='Setup your equipment')
        self.config_button.connect('clicked', self.config_button_on_clicked)
        toolbar.add(self.config_button)

        self.next_step_button = Gtk.ToolButton(sensitive=False,
                                               icon_name='go-next-symbolic',
                                               tooltip_text='Next step')
        self.next_step_button.connect('clicked',
                                      self.next_step_button_on_clicked)
        toolbar.add(self.next_step_button)

        self.brewing_button = Gtk.ToolButton(
            sensitive=False,
            icon_name='media-playback-start-symbolic',
            tooltip_text='Exit setup')

        self.brewing_button.connect('clicked', self.brewing_button_on_clicked)
        toolbar.add(self.brewing_button)

        self.main_box: Gtk.Box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(self.main_box)

        self.appliances_notebook = AppliancesNotebook(self.application, self)
        self.appliances_notebook.show_all()

        self.paned_config = PanedConfig(self.application)
        self.paned_config.show_all()

        self.set_default_size(320, 240)
        self.maximize()

    @log_on_start(logging.INFO, 'Start main window')
    def start(self) -> None:
        self.show_all()

        self.application.refresh_appliances()
        appliance_count = len(self.application.appliances)

        if appliance_count == 0:
            self.show_config()
        else:
            self.show_equipment()

    @log_on_start(logging.INFO, 'Config button clicked')
    def config_button_on_clicked(self, button: Gtk.Button) -> None:
        self.show_config()

    @log_on_start(logging.INFO, 'Show config')
    def show_config(self) -> None:
        self.config_button.hide()
        self.next_step_button.hide()
        self.brewing_button.set_sensitive(True)
        self.brewing_button.show()

        self.main_box.remove(self.appliances_notebook)
        self.main_box.pack_start(self.paned_config, True, True, 0)
        self.paned_config.generate_tree()
        self.paned_config.setup_tree.expand_all()
        self.paned_config.show()

    @log_on_start(logging.INFO, 'Brewing button clicked')
    def brewing_button_on_clicked(self, button: Gtk.Button) -> None:
        if self.paned_config.unsaved_changes:
            confirm = self.show_unsaved_changes()

            if confirm == DialogButtonType.SAVE:
                self.paned_config.save_appliance()
            elif confirm == DialogButtonType.DONT_SAVE:
                self.paned_config.refresh_appliance()
            else:
                return

        self.application.refresh_appliances()
        self.show_equipment()

    @log_on_start(logging.INFO, 'Show unsaved changes dialog')
    @log_on_end(logging.INFO, 'Dialog response: {result}')
    def show_unsaved_changes(self) -> DialogButtonType:
        buttons = DialogButtonType.DONT_SAVE + DialogButtonType.CANCEL

        if self.paned_config.is_valid:
            buttons += DialogButtonType.SAVE

        dialog = ConfirmDialog('Unsaved changes',
                               'You have unsaved changes.',
                               secondary_text='Do you want to save them?',
                               message_type=Gtk.MessageType.WARNING,
                               buttons=buttons,
                               transient_for=self.application.main_window,
                               modal=True)
        confirm = dialog.run()
        dialog.destroy()

        return confirm

    @log_on_start(logging.INFO, 'Show equipment')
    def show_equipment(self) -> None:
        self.config_button.set_sensitive(True)
        self.config_button.show()

        self.next_step_button.set_sensitive(True)
        self.next_step_button.set_tooltip_text('Start brewing')
        self.next_step_button.show()

        self.brewing_button.set_sensitive(False)
        self.brewing_button.hide()

        self.main_box.remove(self.paned_config)
        self.main_box.pack_start(self.appliances_notebook, True, True, 0)
        self.appliances_notebook.refresh_appliances()

    @log_on_start(logging.INFO, 'Next step button clicked')
    def next_step_button_on_clicked(self, button: Gtk.Button) -> None:
        self.application.next_step()

    @log_on_start(logging.INFO, 'Add on read done callback {callback!r}')
    def add_on_read_done(
        self,
        callback: Callable[[Dict[str,
                                 Optional[float]]],
                           None]
    ) -> Callable[[],
                  None]:
        @log_on_start(logging.INFO, 'Remove callback {callback!r}')
        def unsubscribe() -> None:
            self.application.on_read_done_callbacks = tuple(
                keep_callback for keep_callback in self.on_read_done_callbacks
                if keep_callback != callback)

        self.application.on_read_done_callbacks += (callback, )

        return unsubscribe

    @log_on_start(
        logging.INFO,
        'Thermometers readings received. {temperatures}, {duration}, {delay}')
    def thermometers_on_read(self, temperatures, duration, delay) -> None:
        for callback in self.on_read_done_callbacks:
            callback(temperatures)

    @log_on_start(logging.INFO, 'Show brewing in progress dialog')
    @log_on_end(logging.INFO, 'Dialog answer: {result}')
    def show_brewing_in_progress(self) -> DialogButtonType:
        dialog = ConfirmDialog(
            'Brewing session in progress',
            'Do you have a brewing session in progress.',
            secondary_text=(
                "If you exit now you won't be able to restart from the "
                'current step.\nDo you really want to exit?'),
            transient_for=self,
            modal=True)
        do_exit = dialog.run()
        dialog.destroy()
        return do_exit

    @log_on_start(logging.INFO, 'Delete main window event.')
    @log_on_end(logging.INFO, 'Keep window open {result}')
    def do_delete_event(self, event: Gdk.EventAny) -> bool:
        do_exit = None

        if self.paned_config.unsaved_changes:
            do_exit = self.show_unsaved_changes()

        if self.application.current_step != BrewSteps.IDLE:
            do_exit = self.show_brewing_in_progress()

        if do_exit is not None:
            if do_exit == DialogButtonType.SAVE:
                self.paned_config.save_appliance()

            if do_exit in self.exit_cases:
                return False
            else:
                return True

        return False
