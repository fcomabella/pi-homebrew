import logging
from typing import List
from typing import Optional

from gi.repository import Gtk
from gi.repository import Pango
from logdecorator import log_on_start


class NotificationBox(Gtk.Box):
    @log_on_start(logging.INFO, 'Initializing NotificationBox')
    def __init__(self,
                 notification_text: str,
                 icon_name: str = 'dialog-information',
                 icon_size: Gtk.IconSize = Gtk.IconSize.DIALOG,
                 buttons: Optional[List[Gtk.Button]] = None,
                 **kwargs) -> None:

        kwargs['orientation'] = kwargs.get('orientation',
                                           Gtk.Orientation.VERTICAL)
        kwargs['spacing'] = kwargs.get('spacing', 8)
        Gtk.Box.__init__(self, **kwargs)

        if buttons is None:
            self.buttons = []
        else:
            self.buttons = buttons

        center_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                             spacing=kwargs['spacing'])
        self.set_center_widget(center_box)

        icon = Gtk.Image.new_from_icon_name(icon_name, icon_size)
        center_box.pack_start(icon, False, False, 0)

        label = Gtk.Label(max_width_chars=100,
                          width_chars=50,
                          xalign=0.5,
                          justify=Gtk.Justification.LEFT,
                          wrap=True,
                          wrap_mode=Pango.WrapMode.WORD,
                          label=notification_text)

        center_box.pack_start(label, True, True, 0)

        if len(self.buttons) > 0:
            button_box = Gtk.ButtonBox(layout_style=Gtk.ButtonBoxStyle.CENTER)
            center_box.pack_end(button_box, False, False, 0)

            for button in self.buttons:
                button_box.pack_start(button, False, False, 0)
