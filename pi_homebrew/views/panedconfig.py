import logging
from typing import TYPE_CHECKING
from typing import Callable
from typing import Optional
from typing import Tuple
from typing import Type
from typing import Union

import inflect
from gi.repository import Gtk
from logdecorator import log_on_end
from logdecorator import log_on_start

from pi_homebrew.controllers import appliancecontroller
from pi_homebrew.controllers.appliancetypedefs import appliancetypedefs
from pi_homebrew.controllers.dataclasses import ApplianceTypeDef
from pi_homebrew.controllers import appliancecontroller
from pi_homebrew.models import Appliance
from pi_homebrew.models.enum import ApplianceType
from pi_homebrew.models.enum import TabType
from pi_homebrew.views import ConfirmDialog
from pi_homebrew.views import NotificationBox
from pi_homebrew.views.applianceviewtypedefs import applianceviewtypedefs
from pi_homebrew.views.dataclasses import WidgetEventHandler
from pi_homebrew.views.editors.editappliancebox import EditApplianceBox
from pi_homebrew.views.enum import DialogButtonType

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp


class PanedConfig(Gtk.Paned):
    @log_on_end(logging.INFO, 'Paned config initialized')
    def __init__(self, application: 'PiHomebrewApp') -> None:
        Gtk.Paned.__init__(self)

        self.application = application
        self.inflect = inflect.engine()

        self.set_orientation(Gtk.Orientation.HORIZONTAL)

        self.selected_iter: Optional[Gtk.TreeIter] = None

        self.tree_store = Gtk.TreeStore(int, str)

        self.information_icon = Gtk.Image.new_from_icon_name(
            'dialog-information',
            Gtk.IconSize.DIALOG)

        self.warning_icon = Gtk.Image.new_from_icon_name(
            'dialog-warning',
            Gtk.IconSize.DIALOG)

        left_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.pack1(left_box, True, False)

        self.change_right_box_notification(None, None, None)

        toolbar = Gtk.Toolbar(show_arrow=True,
                              toolbar_style=Gtk.ToolbarStyle.ICONS,
                              icon_size=Gtk.IconSize.BUTTON)
        left_box.pack_start(toolbar, False, False, 0)

        self.add_button = Gtk.ToolButton(sensitive=False,
                                         icon_name='document-new-symbolic',
                                         tooltip_text='Add appliance')
        self.add_button.connect('clicked', self.add_button_on_clicked)
        toolbar.add(self.add_button)

        self.delete_button = Gtk.ToolButton(sensitive=False,
                                            icon_name='edit-delete-symbolic',
                                            tooltip_text='Delete appliance')
        self.delete_button.connect('clicked', self.delete_button_on_clicked)
        toolbar.add(self.delete_button)

        self.setup_tree = Gtk.TreeView.new_with_model(self.tree_store)
        self.setup_tree.set_activate_on_single_click(True)
        self.setup_tree.set_enable_tree_lines(True)
        self.setup_tree.set_hexpand(False)
        self.setup_tree.set_headers_visible(False)

        name_renderer = Gtk.CellRendererText()
        name_column = Gtk.TreeViewColumn('Name', name_renderer, text=1)
        name_column.set_min_width(150)
        self.setup_tree.append_column(name_column)

        self.setup_tree.expand_all()

        tree_scroll_window = Gtk.ScrolledWindow(
            hscrollbar_policy=Gtk.PolicyType.AUTOMATIC,
            vscrollbar_policy=Gtk.PolicyType.AUTOMATIC,
            min_content_width=200,
            propagate_natural_width=True)
        tree_scroll_window.add(self.setup_tree)
        left_box.pack_start(tree_scroll_window, True, True, 0)

        self.tree_selection = self.setup_tree.get_selection()
        self.handlers = (WidgetEventHandler(self.tree_selection,
                                            'changed',
                                            self.tree_selection_on_changed),
                         )

    @log_on_end(logging.INFO, 'Sort by property <{prop_name}>')
    def sort_by_prop(
        self,
        prop_name: str = 'name'
    ) -> Callable[[ApplianceTypeDef],
                  Union[None,
                        ApplianceType,
                        str,
                        Type[Appliance],
                        Tuple[TabType,
                              ...]]]:
        log_msg = f'{prop_name} value: '

        @log_on_end(logging.INFO, log_msg + '{result:s}.')
        def get_prop(
            item: Optional[ApplianceTypeDef]
        ) -> Union[None,
                   ApplianceType,
                   str,
                   Type[Appliance],
                   Tuple[TabType,
                         ...]]:
            return getattr(item, prop_name, None)

        return get_prop

    @log_on_start(logging.INFO, 'Disconnect event handlers.')
    def disconnect_event_handlers(self) -> None:
        for handler in self.handlers:
            if handler.handler_id is not None:
                handler.widget.disconnect(handler.handler_id)

    @log_on_start(logging.INFO, 'Connect event handlers.')
    def connect_event_handlers(self) -> None:
        for handler in self.handlers:
            handler.handler_id = handler.widget.connect(
                handler.event_name,
                handler.handler_func)

    @log_on_start(logging.INFO, 'Generate appliances tree.')
    def generate_tree(self) -> None:
        self.tree_selection.unselect_all()
        self.disconnect_event_handlers()
        self.tree_selection.set_select_function(lambda *args, **kwargs: True)
        self.tree_store.clear()

        root_iters = (self.tree_store.append(
            None,
            [TabType.BREW_HOUSE,
             'Brew house']),
                      self.tree_store.append(
                          None,
                          [TabType.FERMENTATION,
                           'Fermentation']))

        sorted_appliance_types = sorted(
            [appliance_type for appliance_type in appliancetypedefs],
            key=self.sort_by_prop('name'))

        for root_iter in root_iters:
            parent_tab_type = self.tree_store.get_value(root_iter, 0)
            appliance_types = [
                appliance_type_info
                for appliance_type_info in sorted_appliance_types
                if parent_tab_type in appliance_type_info.on_tabs
            ]

            for appliance_type in appliance_types:
                type_name = self.inflect.plural(
                    appliance_type.name).capitalize()
                type_iter = self.tree_store.append(
                    root_iter,
                    [appliance_type.value,
                     type_name])

                appliances = appliancecontroller.get_appliances(
                    self.application.db_session,
                    appliance_type.value,
                    parent_tab_type)

                for appliance in appliances:
                    self.tree_store.append(type_iter,
                                           [appliance.id,
                                            appliance.name])

        self.connect_event_handlers()
        self.tree_selection.set_select_function(self.can_change_selection)

    def tree_selection_on_changed(self, selection: Gtk.TreeSelection) -> None:
        (model, new_iter) = selection.get_selected()
        self.update_add_button_status(model, new_iter)
        self.update_delete_button_status(model, new_iter)
        self.update_right_box(model, new_iter, self.selected_iter)

        if (self.selected_iter is not None
                and model.iter_depth(self.selected_iter) == 2
                and model.get_value(self.selected_iter,
                                    0) == -1):
            model.remove(self.selected_iter)

        self.selected_iter = new_iter

    def update_add_button_status(self,
                                 model: Gtk.TreeStore,
                                 new_iter: Optional[Gtk.TreeIter]) -> None:
        if new_iter is not None:
            new_depth = model.iter_depth(new_iter)

            if new_depth == 0:
                self.add_button.set_sensitive(False)
            elif new_depth == 1:
                self.add_button.set_sensitive(True)
            elif new_depth == 2:
                new_id = model.get_value(new_iter, 0)
                self.add_button.set_sensitive(new_id != -1)
        else:
            self.add_button.set_sensitive(False)

    def update_delete_button_status(self,
                                    model: Gtk.TreeStore,
                                    new_iter: Optional[Gtk.TreeIter]) -> None:
        if new_iter is not None:
            new_depth = model.iter_depth(new_iter)
            self.delete_button.set_sensitive(new_depth == 2)
        else:
            self.delete_button.set_sensitive(False)

    def change_right_box_notification(self,
                                      tab_type: Optional[TabType],
                                      appliance_type: Optional[ApplianceType],
                                      appliance_id: Optional[int]) -> None:
        right_box = self.get_child2()

        if right_box is not None:
            right_box.destroy()

        if tab_type is None:
            right_box = NotificationBox((
                'Here you can setup your equipment. Select an option from the '
                'tree to start adding appliances.'))
        elif appliance_type is None:
            notification_texts = (
                ('You have selected the "Brew House" tab. In this tab you can '
                 'add actuators, heat exchangers and kettles.'),
                ('You have selected the "Fermentors" tab.  In this tab you '
                 'can add fermentors, actuators and kettles.'))

            right_box = NotificationBox(notification_texts[tab_type])
        else:
            doc_description = '\n'.join([
                line.strip() for line in applianceviewtypedefs[appliance_type].
                docstring.partition('Attributes')[0].splitlines()
                if len(line) > 0 and not line.isspace()
            ])
            editor_class = applianceviewtypedefs[appliance_type].editor_class
            name = applianceviewtypedefs[appliance_type].name

            self.add_button.set_sensitive(editor_class is not None)
            message = (f'{self.inflect.plural(name).capitalize()}.\n\n')
            icon_name = 'dialog-information'
            buttons = []

            if editor_class is None:
                message += 'This appliance type is not implemented yet.'
                icon_name = 'dialog-warning'
            else:
                message += (
                    f'{doc_description}\n\n'
                    f'Click the "Add {name}" button below to add a new '
                    f'{name.lower()}.\n\n'
                    f'If you want to edit an already existing {name.lower()}, '
                    'just select it on the tree to the left.')
                save_icon = Gtk.Image.new_from_icon_name(
                    'document-new-symbolic',
                    Gtk.IconSize.BUTTON)
                add_kettle_button = Gtk.Button(label=f'Add {name.lower()}',
                                               halign=Gtk.Align.CENTER,
                                               always_show_image=True,
                                               image=save_icon)
                add_kettle_button.connect('clicked',
                                          self.add_button_on_clicked)
                buttons = [add_kettle_button]

            right_box = NotificationBox(message, icon_name, buttons=buttons)

        self.pack2(right_box, True, False)
        right_box.show_all()

    def change_right_box_editor(self,
                                appliance_type: Optional[ApplianceType]
                                ) -> None:
        editor_class = applianceviewtypedefs[appliance_type].editor_class

        if editor_class is not None:
            right_box = self.get_child2()

            if right_box is not None:
                right_box.destroy()

            right_box = editor_class(self.application, self)

            self.pack2(right_box, True, False)
            right_box.show_all()

    def get_appliance_props(
        self,
        model: Gtk.TreeStore,
        iter: Gtk.TreeIter
    ) -> Tuple[Optional[TabType],
               Optional[ApplianceType],
               Optional[int]]:
        scan_iter = iter

        props: Tuple[Optional[TabType],
                     Optional[ApplianceType],
                     Optional[int]] = (None,
                                       None,
                                       None)

        while scan_iter is not None:
            depth = model.iter_depth(scan_iter)
            prop = model.get_value(scan_iter, 0)
            props = props[:depth] + (
                prop,
            ) + props[depth + 1:]  # type:ignore [assignment] # noqa E481
            scan_iter = model.iter_parent(scan_iter)

        return props

    def update_right_box(self,
                         model: Gtk.TreeStore,
                         new_iter: Optional[Gtk.TreeIter],
                         old_iter: Optional[Gtk.TreeIter]) -> None:
        if new_iter != old_iter:
            new_tab_type = None
            new_appliance_type = None
            new_appliance_id = None

            old_tab_type = None
            old_appliance_type = None
            old_appliance_id = None

            if new_iter is not None:
                (new_tab_type,
                 new_appliance_type,
                 new_appliance_id) = self.get_appliance_props(model,
                                                              new_iter)

            if old_iter is not None:
                (old_tab_type,
                 old_appliance_type,
                 old_appliance_id) = self.get_appliance_props(model,
                                                              old_iter)

            if new_appliance_id is None:
                self.change_right_box_notification(new_tab_type,
                                                   new_appliance_type,
                                                   new_appliance_id)
            else:
                if (new_appliance_id != old_appliance_id):
                    self.change_right_box_editor(new_appliance_type)

                if new_appliance_id == -1:
                    appliance_count = appliancecontroller.count_appliances(
                        self.application.db_session,
                        new_appliance_type,
                        new_tab_type)
                    appliance_type_info = appliancetypedefs[new_appliance_type]
                    appliance = appliance_type_info.model_class(
                        on_tab=new_tab_type,
                        name=(f'{appliance_type_info.name.capitalize()} '
                              f'{appliance_count+1}'))
                else:
                    appliance = appliancecontroller.get_appliance(
                        self.application.db_session,
                        new_appliance_id)

                right_box = self.get_child2()
                right_box.set_appliance(appliance)

                if new_appliance_id == -1:
                    model.set_row(new_iter, [-1, f'<New> {appliance.name}'])

    def add_button_on_clicked(self, button: Gtk.Button) -> None:
        if self.selected_iter is not None:
            selected_depth = self.tree_store.iter_depth(self.selected_iter)

            if selected_depth == 1:
                appliance_type_iter = self.selected_iter
            else:
                appliance_type_iter = self.tree_store.iter_parent(
                    self.selected_iter)

            new_iter = self.tree_store.append(appliance_type_iter, [-1, ''])

            self.setup_tree.expand_row(
                self.tree_store.get_path(appliance_type_iter),
                True)
            self.tree_selection.select_iter(new_iter)

    def delete_button_on_clicked(self, button: Gtk.Button) -> None:
        (tab_type,
         appliance_type,
         appliance_id) = self.get_appliance_props(self.tree_store,
                                                  self.selected_iter)
        name = self.tree_store.get_value(self.selected_iter, 1)
        type_name = appliancetypedefs[appliance_type].name
        dialog = ConfirmDialog(
            f'Delete {type_name}?',
            f'Do you want to delete the {type_name} "{name}"?',
            transient_for=self.application.main_window,
            modal=True)

        confirmation = dialog.run()
        dialog.destroy()

        if confirmation == DialogButtonType.YES:
            if appliance_id != -1:
                appliance = appliancecontroller.get_appliance(
                    self.application.db_session,
                    appliance_id)
                appliancecontroller.delete_appliance(
                    self.application.db_session,
                    appliance)

            iter = self.selected_iter
            self.tree_selection.unselect_iter(iter)
            self.tree_store.remove(iter)

    def appliance_on_change(self, appliance: Appliance) -> None:
        if self.selected_iter is not None:
            right_pane = self.get_child2()

            if not isinstance(right_pane, NotificationBox):
                if appliance.id is None:
                    appliance_id = -1
                    appliance_name = f'<New> {appliance.name}'
                else:
                    appliance_id = appliance.id
                    if right_pane.unsaved_changes is True:
                        appliance_name = f'<Modified> {appliance.name}'
                    else:
                        appliance_name = appliance.name

                self.tree_store.set_row(self.selected_iter,
                                        [appliance_id,
                                         appliance_name])

    def can_change_selection(self,
                             selection: Gtk.TreeSelection,
                             model: Gtk.TreeModel,
                             path: Gtk.TreePath,
                             path_currently_selected: bool,
                             *data: None) -> bool:
        right_pane = self.get_child2()

        if (not isinstance(right_pane,
                           NotificationBox) and path_currently_selected is True
                and right_pane.unsaved_changes):
            confirm = self.application.main_window.show_unsaved_changes()

            if confirm == DialogButtonType.SAVE:
                right_pane.save_appliance()
            elif confirm == DialogButtonType.DONT_SAVE:
                right_pane.refresh_appliance()
                iter = model.get_iter(path)
                model.set_value(iter, 1, right_pane.appliance.name)
            else:
                return False

        return True

    @property
    def unsaved_changes(self) -> bool:
        right_pane = self.get_child2()

        if isinstance(right_pane, NotificationBox):
            return False

        return right_pane.unsaved_changes

    @property
    def is_valid(self) -> bool:
        right_pane = self.get_child2()

        if isinstance(right_pane, NotificationBox):
            return True

        return right_pane.is_valid

    def save_appliance(self) -> Optional[Appliance]:
        right_pane = self.get_child2()

        if not isinstance(right_pane, NotificationBox):
            return right_pane.save_appliance()

    def refresh_appliance(self) -> Optional[Appliance]:
        right_pane: Union[NotificationBox,
                          EditApplianceBox] = self.get_child2()

        if not isinstance(right_pane, NotificationBox):
            appliance = right_pane.refresh_appliance()

            if self.selected_iter is not None:
                selected_appliance = self.get_appliance_props(
                    self.tree_store,
                    self.selected_iter)

                if selected_appliance == (appliance.on_tab,
                                          appliance.appliance_type,
                                          appliance.id):
                    self.tree_store.set_value(self.selected_iter,
                                              1,
                                              appliance.name)

            return appliance
