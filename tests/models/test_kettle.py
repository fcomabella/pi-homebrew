import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.session import Session

from pi_homebrew.models import Kettle
from pi_homebrew.models.enum import ApplianceType
from pi_homebrew.models.enum import TabType


def test_correct(db_session: Session) -> None:
    new_kettle = Kettle(name='Test kettle',
                        on_tab=TabType.BREW_HOUSE,
                        description='Test kettle description')
    db_session.add(new_kettle)
    db_session.commit()

    assert new_kettle.id is not None
    assert new_kettle.name == 'Test kettle'
    assert new_kettle.on_tab == TabType.BREW_HOUSE
    assert new_kettle.appliance_type == ApplianceType.KETTLE
    assert new_kettle.show_all_thermometers is True
    assert new_kettle.show_average_temp is True
    assert new_kettle.thermometers == []


def test_must_have_on_tab(db_session: Session) -> None:
    with pytest.raises(IntegrityError) as excinfo:
        new_kettle = Kettle(name='Test kettle',
                            description='Test kettle description')
        db_session.add(new_kettle)
        db_session.commit()

    assert 'NOT NULL constraint failed: appliances.on_tab' in \
        str(excinfo.value)


def test_name_not_null(db_session: Session) -> None:
    with pytest.raises(IntegrityError) as excinfo:
        new_appliance = Kettle(description='Test appliance description',
                               on_tab=TabType.BREW_HOUSE)
        db_session.add(new_appliance)
        db_session.commit()

    assert 'NOT NULL constraint failed: appliances.name' in str(excinfo.value)


def test_name_not_empty_string(db_session: Session) -> None:
    with pytest.raises(IntegrityError) as excinfo:
        new_appliance = Kettle(name='',
                               description='Test appliance description',
                               on_tab=TabType.BREW_HOUSE)
        db_session.add(new_appliance)
        db_session.commit()

    assert 'CHECK constraint failed: appliances' in str(excinfo.value)


def test_unique_kettle_name(db_session: Session) -> None:
    kettle1 = Kettle(name='Test kettle', on_tab=TabType.BREW_HOUSE)
    kettle2 = Kettle(name='Test kettle', on_tab=TabType.BREW_HOUSE)

    with pytest.raises(IntegrityError) as excinfo:
        db_session.add_all([kettle1, kettle2])
        db_session.commit()

    assert ('UNIQUE constraint failed: appliances.on_tab, '
            'appliances.name') in str(excinfo.value)


def test_different_tabs_same_name(db_session: Session) -> None:
    kettle1 = Kettle(on_tab=TabType.BREW_HOUSE, name='Test kettle')
    kettle2 = Kettle(on_tab=TabType.FERMENTATION, name='Test kettle')

    db_session.add_all([kettle1, kettle2])
    db_session.commit()


def test_kettle_repr(db_session: Session) -> None:
    new_kettle = Kettle(on_tab=TabType.BREW_HOUSE,
                        name='Test appliance',
                        description='Test appliance description')
    db_session.add(new_kettle)
    db_session.commit()

    assert repr(new_kettle) == (f'<Kettle ('
                                f'id={new_kettle.id}, '
                                f'on_tab={TabType.BREW_HOUSE}, '
                                f'name=Test appliance, '
                                f'description=Test appliance description, '
                                f'type={ApplianceType.KETTLE}, '
                                f'show_all_thermometers=True, '
                                f'show_average_temp=True'
                                f')>')
