import pi_homebrew


def test_version() -> None:
    assert pi_homebrew.__version__ == '0.1.2'
